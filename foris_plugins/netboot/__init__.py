#
# foris-netboot-plugin
# Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import bottle
import os


from foris import fapi, validators
from foris.config import ConfigPageMixin, add_config_page
from foris.config_handlers import BaseConfigHandler
from foris.form import Textbox
from foris.plugins import ForisPlugin
from foris.state import current_state
from foris.utils import messages
from foris.utils.translators import (
    gettext_dummy as gettext,
    gettext as _,
)
from foris.utils.routing import reverse


class NetbootPluginConfigHandler(BaseConfigHandler):
    STATE_ACCEPTED = gettext("accepted")
    STATE_INCOMMING = gettext("incomming")

    userfriendly_title = gettext("Netboot")

    def get_form(self):
        #data = current_state.backend.perform("netboot", "get_settings")

        #if self.data:
        #    # Update from post (used when the form is updated via ajax)
        #    data.update(self.data)

        form = fapi.ForisForm("netboot", {})
        section = form.add_section(
            name="main_section",
            title=self.userfriendly_title,
        )

        def form_cb(data):
            return "save_result", {}

        form.add_callback(form_cb)
        return form

    def get_serial_form(self, data=None):
        generate_serial_form = fapi.ForisForm("serial_form", data)
        serial_section = generate_serial_form.add_section("serial_section", title=None)
        serial_section.add_field(
            Textbox, name="serial", label=" ", required=True,
            validators=[validators.MacAddress()],
        )
        return generate_serial_form


# This represents a plugin page
class NetbootPluginPage(ConfigPageMixin, NetbootPluginConfigHandler):
    slug = "netboot"
    menu_order = 60
    template = "netboot/netboot"
    template_type = "jinja2"

    def save(self, *args, **kwargs):
        # Handle form result here
        return super(NetbootPluginPage, self).save(*args, **kwargs)

    def _prepare_render_args(self, args):
        args['PLUGIN_NAME'] = NetbootPlugin.PLUGIN_NAME
        args['PLUGIN_STYLES'] = NetbootPlugin.PLUGIN_STYLES
        args['PLUGIN_STATIC_SCRIPTS'] = NetbootPlugin.PLUGIN_STATIC_SCRIPTS
        args['PLUGIN_DYNAMIC_SCRIPTS'] = NetbootPlugin.PLUGIN_DYNAMIC_SCRIPTS

    def render(self, **kwargs):
        self._prepare_render_args(kwargs)
        return super(NetbootPluginPage, self).render(**kwargs)

    def _action_list(self):
        res = current_state.backend.perform("netboot", "list")

        return bottle.template(
            "netboot/_records.html.j2",
            records=res["devices"],
            template_adapter=bottle.Jinja2Template,
        )

    def _action_generic(self, action):
        if bottle.request.method != 'POST':
            messages.error(_("Wrong HTTP method."))
            bottle.redirect(reverse("config_page", page_name="remote"))
        form = self.get_serial_form(bottle.request.POST.decode())
        if not form.data["serial"]:
            raise bottle.HTTPError(404, "serial not found")

        res = current_state.backend.perform(
            "netboot", action, {"serial": form.data["serial"]}
        )

        bottle.response.set_header("Content-Type", "application/json")
        return res

    def _action_revoke(self):
        return self._action_generic("revoke")

    def _action_accept(self):
        return self._action_generic("accept")

    def call_ajax_action(self, action):
        if action == "list":
            return self._action_list()
        if action == "revoke":
            return self._action_revoke()
        if action == "accept":
            return self._action_accept()

        raise ValueError("Unknown AJAX action.")


# plugin definition
class NetbootPlugin(ForisPlugin):
    PLUGIN_NAME = "netboot"  # also shown in the url
    DIRNAME = os.path.dirname(os.path.abspath(__file__))

    PLUGIN_STYLES = [
        "css/netboot.css",  # path to css script generated using sass/netboot.sass
    ]
    PLUGIN_STATIC_SCRIPTS = [
        "js/contrib/Chart.bundle.min.js",  # 3rd party static js
        "js/netboot.js",  # static js file
    ]
    PLUGIN_DYNAMIC_SCRIPTS = [
        "netboot.js",  # dynamic js file (a template which will be rendered to javascript)
    ]

    def __init__(self, app):
        super(NetbootPlugin, self).__init__(app)
        add_config_page(NetbootPluginPage)
