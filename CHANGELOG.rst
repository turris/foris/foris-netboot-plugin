0.3 (2019-03-08)
----------------

* js WS api changed

0.2 (2019-01-30)
----------------

* don't display empty table show some message instead
* using serial instead of macaddress

0.1 (2019-01-25)
----------------

* initial version
