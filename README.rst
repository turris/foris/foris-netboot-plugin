**DEPRECATED netboot functionality moved to https://gitlab.labs.nic.cz/turris/foris/foris-subordinates-plugin**

Foris netboot plugin
===================
This is a netboot plugin for foris

Requirements
============

* foris
* foris-controller-netboot-module

Installation
============

    ``python setup.py install``

    or

    ``pip install .``
